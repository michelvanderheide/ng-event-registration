export const environment = {
  production: true,
  apiUrl: 'https://api.hardloopscholing.nl',
  maxSubscriptions: 16
};
