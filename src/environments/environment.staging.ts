export const environment = {
  production: true,
  apiUrl: 'https://staging.api.hardloopscholing.nl',
  maxSubscriptions: 16
};
