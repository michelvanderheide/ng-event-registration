import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { DrillsRoutingModule } from './drills-routing.module';
import { LayoutComponent } from './layout.component';
import { ListComponent } from './list.component';
import { AddEditComponent } from './add-edit.component';
import { EditorModule } from '@tinymce/tinymce-angular';


@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        DrillsRoutingModule,
        FormsModule,
        EditorModule
    ],
    declarations: [
        LayoutComponent,
        ListComponent,
        AddEditComponent
    ]
})
export class DrillsModule { }