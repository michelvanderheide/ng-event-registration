﻿import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { DrillService } from '@app/_services';
import { Drill, Tag } from '@app/_models';


import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({ templateUrl: 'list.component.html', styleUrls: ['./list.component.less'] })
export class ListComponent implements OnInit {
    isDeleting = false;
    closeResult = '';
    tagName: string;
    filterActive:boolean = false;
    public searchText = '';
    private drillsFound:number = 0;
    public name = '';
    public activeTags: string[];
    public tags: Tag[];
    constructor(public drillService: DrillService, private modalService: NgbModal) {
    }

    ngOnInit() {

        this.drillService.drills = this.drillService.getCachedDrills();
        this.drillService.getDrills()
            .pipe(first())
            .subscribe(drills => {
                localStorage.setItem('drills', JSON.stringify(drills));
                console.log('drills:',drills);
                this.drillService.setDrills(drills);
                this.filterOnTags();
                this.filterOnText(this.searchText);
                this.filterActive = true;
            });

        this.drillService.getTags()
            .pipe(first())
            .subscribe(tags => {
                console.log('tags:',tags);
                this.setTags(tags);
                this.filterOnTags();
                this.filterOnText(this.searchText);
            });
    }

    open(content) {
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    } 
    
    newDrill() {
        console.log('newDrill');
    }

    searchDrill() {
        console.log('txt:',this.searchText)
        this.filterOnText(this.searchText)
    }
    clearSearch() {
        this.searchText = '';
        this.searchDrill();
    }

    toggleVideo(drill: Drill) {
        console.log('show/hide video:', drill)
        drill.showVideo = !drill.showVideo;
    }

    deleteDrill(idx: number) {
        console.log('delete:', this.drillService.drills[idx]);
        this.drillService.deleteDrill(this.drillService.drills[idx].id).subscribe(function(){
            console.log('deleted');
        });
        delete this.drillService.drills[idx];
    }


    setTags(tags: Tag[]) {
        tags.map(t => t.active = (typeof t.active == 'undefined') ? false : t.active);
        this.tags = tags;
        localStorage.setItem("tags", JSON.stringify(this.tags));
    }

	toggleTag(idx: number) {
        this.tags[idx].active = (!this.tags[idx].active);
        localStorage.setItem("tags", JSON.stringify(this.tags));
        this.filterOnTags();
    }  

    doFilterCount() {
        this.drillsFound = 0;
        this.drillService.drills.map(d => {
            if (d.includesText && d.includesTag) {
                this.drillsFound++;
            }
        });
    }

    filterOnText(searchText: string) {
        
        searchText = searchText.toLowerCase();
        let showAll = false;
        if (searchText.length < 4) {
            showAll = true;
        }
        this.drillService.drills.map(d => { 
            d.includesText = showAll || d.title.toLowerCase().includes(searchText);
        });
        this.doFilterCount();

    }
    
    filterOnTags() {
        this.activeTags = [];
        this.tags.filter((t) => {
            return t.active == true;
        }).map(t => this.activeTags.push(t.TagName));

        console.log('activeTags:', this.activeTags);
        let showAll = this.activeTags.length == 0;

        this.drillService.drills.map(d => {
            console.log('check drill', d.title);
            if (d.tags) {
                d.includesTag =d.tags.some(t => {
                    console.log('include:'+d.title);
                    return showAll || this.activeTags.includes(t);
                })
            }
        });
        this.doFilterCount();
    }          
}