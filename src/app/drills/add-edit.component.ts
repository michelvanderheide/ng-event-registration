﻿import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DrillService, AlertService } from '@app/_services';
import { Tag } from '@app/_models';
import { first } from 'rxjs/operators';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({ templateUrl: 'add-edit.component.html' })
export class AddEditComponent implements OnInit {
    form: FormGroup;
    id: number;
    isAddMode: boolean;
    loading = false;
    submitted = false;
    allTags: Tag[];
    closeResult = '';
    tagsActive: boolean = false;
    youtubeUrl: SafeResourceUrl;
    hasYoutubeUrl: boolean = false;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private drillService: DrillService,
        private alertService: AlertService,
        private modalService: NgbModal,
    ) {}

    ngOnInit() {
        this.id = this.route.snapshot.params['id'];
        console.log('id:',this.id)
        this.isAddMode = this.id == 0;
        console.log('addmode',this.isAddMode);
        this.allTags = [];
        this.form = this.formBuilder.group({
            title: ['', Validators.required],
            description: ['', Validators.required],
            videoUrl: [''],
            tags: []
        });

        
        if (!this.isAddMode) {
            
            console.log('set:', this.drillService.getDrillById(this.id))
            this.drillService.drillValue = this.drillService.getDrillById(this.id);
            this.youtubeUrl = this.drillService.getYoutubeUrl(this.drillService.drillValue.videoUrl);
            console.log('this.drillService.drillValue',this.drillService.drillValue)
            this.form.patchValue(this.drillService.drillValue);
            
        } else {
            this.allTags = [];
        }
        this.drillService.getTags().subscribe(tags => {
            this.allTags = tags;
            console.log('this.drillService.drillValue.tags:', this.drillService.drillValue.tags);
            console.log('this.allTags:', this.allTags);
            this.drillService.drillValue.tags.map(t => {
                this.allTags.map(at =>{
                    console.log('check-1',at)
                    console.log('check-2',t)
                    if (at.TagName === t) {
                        console.log('set active',t)
                        at.active = true;
                    }
                })
            });
            this.tagsActive = true;
        });
    }


    open(content) {
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
            this.drillService.drillValue.tags = [];
            this.allTags.filter(tag => tag.active===true).map(tag => {
                this.drillService.drillValue.tags.push(tag.TagName);
            });
            console.log("saved tags:",this.drillService.drillValue.tags);
            this.form.controls.tags.setValue(this.drillService.drillValue.tags);
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }
    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {

            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    toggleTag(idx: number) {
        this.allTags[idx].active = (!this.allTags[idx].active);
    }

    setYoutubeUrl(youtubeId) {
        this.hasYoutubeUrl = youtubeId !== '';
        if (this.hasYoutubeUrl ) {
            this.youtubeUrl = this.drillService.getYoutubeUrl(youtubeId);
        }
    }

    // convenience getter for easy access to form fields
    get f() { return this.form.controls; }

    onSubmit() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.form.invalid) {
            return;
        }

        this.loading = true;
        if (this.isAddMode) {
            this.createDrill();
        } else {
            this.updateDrill();
        }
    }

    private createDrill() {
        this.form.value.id = this.id;
        this.form.value.tags = this.allTags;
        this.drillService.createDrill(this.form.value)
            .pipe(first())
            .subscribe({
                next: () => {
                    this.alertService.success('Drill added successfully', { keepAfterRouteChange: true });
                    this.router.navigate(['../../'], { relativeTo: this.route });
                },
                error: error => {
                    this.alertService.error(error);
                    this.loading = false;
                }
            });
    }

    private updateDrill() {
        this.form.value.id = this.id;
        this.form.value.hasVideo = this.form.value.videoUrl !== '';
        this.drillService.saveDrill(this.form.value)
            .pipe(first())
            .subscribe({
                next: () => {
                    this.alertService.success('Update successful', { keepAfterRouteChange: true });
                    this.loading = false;
                    this.drillService.setCachedDrill(this.form.value);
                    this.router.navigate(['../../'], { relativeTo: this.route });
                },
                error: error => {
                    this.alertService.error(error);
                    this.loading = false;
                }
            });
    }
}