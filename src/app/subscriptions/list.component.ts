﻿import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { SubscriptionService } from '@app/_services';
import { AccountService } from '@app/_services';
import { listLazyRoutes } from '@angular/compiler/src/aot/lazy_routes';
import { Subscription } from '@app/_models';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from '@environments/environment';

@Component({ templateUrl: 'list.component.html' })
export class ListComponent implements OnInit {
    subscriptions = null;
    list = null;
    isDeleting = false;

    constructor(private subscriptionService: SubscriptionService, 
        private accountService: AccountService,
        private route: ActivatedRoute,
        private router: Router,
        ) {}

    ngOnInit() {
        var user = this.accountService.getUser();
        this.subscriptionService.getSubscriptionsForGroup(user.rungroup)
            .pipe(first())
            .subscribe(subscriptions => {
                console.log('subscriptions:',subscriptions);
                this.subscriptions = subscriptions

                this.list = [];
                var listItems = this.list;

                console.log('user:', user);
                var hasSubscription = false;
                
                this.subscriptions.forEach(function(subscription, i){
                    console.log('sub:',subscription)

                    subscription.isSubscribed = true;
                    subscription.isEnabled = false;

                    let idx = listItems.findIndex(j => j.SessionFk === subscription.SessionFk)
                    
                    if (idx<0) {

                        console.log('push:', subscription)
                        listItems.push({ SessionFk: subscription.SessionFk, SessionName: subscription.SessionName, 
                                MaxSubscribers: subscription.MaxSubscribers, subscriptions: [] });
                        idx = listItems.length-1;

                        hasSubscription = false;
                    } else {
                        hasSubscription = true;
                    }

                    if (subscription.AccountFk == user.id) {
                        subscription.isSubscribed = true;
                        subscription.isEnabled = true;
                    }

                    //if (listItems[idx].subscriptions.length < listItems[idx].MaxSubscribers) {
                        listItems[idx].subscriptions.push(subscription);
                    //}
                    
                });
                console.log('listItems', listItems)


                listItems.forEach(function(listItem, i){
                    if (!listItem.subscriptions.find(e => e.AccountFk === user.id)) {

                        console.log('Max.',listItem.MaxSubscribers)
                        
                        if (listItems[i].subscriptions.length < listItem.MaxSubscribers) {

                            let newSubscription = new Subscription();
                            newSubscription.SessionAccountPk = 0;
                            newSubscription.isSubscribed = false;
                            newSubscription.isEnabled = true;
                            newSubscription.SessionFk = listItem.SessionFk;
                            newSubscription.SessionName = listItem.SessionName;
                            newSubscription.AccountFk = parseInt(user.id);
                            newSubscription.AccountName = user.name;

                            listItems[i].subscriptions.push(newSubscription);
                        }
                    }
                });
                console.log('list:',this.list)
            });
    }

    // todo
    deleteSubscription(SessionAccountPk: string) {
        const subscription = this.subscriptions.find(x => x.SessionAccountPk === SessionAccountPk);
        console.log(this.subscriptions)
        console.log('SessionAccountPk:',SessionAccountPk)
        subscription.isChanging = true;
        this.subscriptionService.unsubscribe(subscription, SessionAccountPk)
            .pipe(first())
            .subscribe((s: Subscription) => {
                console.log('set...',s)
                s.isSubscribed = false;
            });
    }

    // todo
    addSubscription(subscription: Subscription) {
        subscription.isChanging = true;
        this.subscriptionService.subscribe(subscription)
            .pipe(first())
            .subscribe((s: Subscription) => {
                console.log("added:",s)
                subscription.SessionAccountPk = s.SessionAccountPk;
                subscription.isSubscribed = true;
                this.subscriptions.push(subscription);
            });
    }
}