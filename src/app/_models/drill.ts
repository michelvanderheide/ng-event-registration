﻿export class Drill {
    id: number;
    title: string;
    description: string;
    hasImage: boolean;
    hasVideo: boolean;
    imgUrl: string;
    videoUrl: string;
    showVideo: boolean;
    tags: string[];
    includesTag: boolean;
    includesText: boolean;
}


export class Tag {
    TagPk: number;
    TagName: string;
    active: boolean;
}
