﻿export class User {
    id: string;
    email: string;
    password: string;
    name: string;
    token: string;
    rungroup: string;
    admin: boolean;
}