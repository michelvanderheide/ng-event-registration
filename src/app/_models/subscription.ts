export class Subscription {
    SessionAccountPk: number;
    SessionFk: number;
    SessionName: string;
    SessionDate: string;
    AccountFk: number
    AccountName: string;
    SortOrder: string;
    MaxSubscribers: number;
    isSubscribed: boolean;
    isEnabled: boolean;
    isChanging: boolean;
}


export class Rungroup {
    RungroupPk: number;
    RungroupName: string;
}