﻿import { Component } from '@angular/core';

import { User } from '@app/_models';
import { AccountService, DrillService } from '@app/_services';
import { environment } from '@environments/environment';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent {
    user: User;
    environment: any;

    constructor(private accountService: AccountService, private drillService: DrillService) {
        this.user = this.accountService.userValue;
        this.environment = environment;
        this.drillService.adminMode = this.user.admin;
    }
    logout() {
        this.accountService.logout();
    }
}