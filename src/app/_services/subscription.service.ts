﻿import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '@environments/environment';
import { Subscription, Rungroup } from '@app/_models';

@Injectable({ providedIn: 'root' })
export class SubscriptionService {
    private subscriptionSubject: BehaviorSubject<Subscription>;
    public subscription: Observable<Subscription>;

    constructor(
        private router: Router,
        private http: HttpClient
    ) {
        this.subscriptionSubject = new BehaviorSubject<Subscription>(JSON.parse(localStorage.getItem('subscription')));
        this.subscription = this.subscriptionSubject.asObservable();
    }

    public get subscriptionValue(): Subscription {
        return this.subscriptionSubject.value;
    }


    getSubscriptionsForGroup(id: string) {
        console.log('get all...');
        return this.http.get<Subscription[]>(`${environment.apiUrl}/sessionaccounts/${id}`);
    }

    getRungroups() {
        console.log('get rungroups...');
        return this.http.get<Rungroup[]>(`${environment.apiUrl}/groups/`);

    }
    subscribe(subscsription: Subscription) {
        console.log('subscribe:',subscsription)
        return this.http.post(`${environment.apiUrl}/sessionaccount`, subscsription)
            .pipe(map(x => {
                console.log('x:', subscsription)
                return x;
            }));
    }

    unsubscribe(subscription: Subscription, id: string) {
        return this.http.delete(`${environment.apiUrl}/sessionaccount/${id}`)
            .pipe(map(x => {
                console.log('unsubscribe...', x);
                return subscription;
            }));
    }    

}