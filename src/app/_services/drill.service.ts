﻿import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';

import { environment } from '@environments/environment';
import { Drill, Tag } from '@app/_models';
import { DomSanitizer } from '@angular/platform-browser';
import { AccountService } from '@app/_services';

@Injectable({ providedIn: 'root' })
export class DrillService {
    private drillSubject: BehaviorSubject<Drill>;
    public drill: Observable<Drill>;
    public drills: Drill[]; 
    public tags: Tag[];
    public adminMode: boolean;
    
    constructor(
        private router: Router,
        private http: HttpClient,
        private sanitizer: DomSanitizer,
        private accountService: AccountService
    ) {
        console.log('localStorage.getItem', localStorage.getItem('drill'));
        let drill = new Drill();
        if (localStorage.getItem('drill') !== 'undefined') {
            drill = JSON.parse(localStorage.getItem('drill'));
        }
        this.drillSubject = new BehaviorSubject<Drill>(drill);
        this.drill = this.drillSubject.asObservable();
        console.log('get cached drills');
        this.drills = this.getCachedDrills();
        this.adminMode = this.accountService.userValue.admin;

    }

    public get drillValue(): Drill {
        return this.drillSubject.value;
    }

    public set drillValue(value: Drill) {
        console.log("drillValue:", value)
        localStorage.setItem('drill', JSON.stringify(value));
        this.drillSubject.next(value);
    }

    getTags() {
        const tags = JSON.parse(localStorage.getItem('tags'));
        console.log('get all tags...', tags);

        if (tags == null) {
            return this.http.get<Tag[]>(`${environment.apiUrl}/tags/`);
        } else {
            return of(tags);
        }
    }
    getYoutubeUrl(videoUrl: string) {
        console.log('getYoutubeUrl')
        return this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/'+videoUrl);
    }
    getDrills() {
        console.log('get all drills...');
        let drills = this.http.get<Drill[]>(`${environment.apiUrl}/drills/`);
        return drills;
    }

    getCachedDrills() {
        let drills = [];
        if (localStorage.getItem('drills') !== null) {
            drills = this.drills = JSON.parse(localStorage.getItem('drills'));
        }
        return drills;
    }

    setDrills(drills: Drill[]) {
        this.drills = drills;
        localStorage.setItem('drills', JSON.stringify(drills));
    }

    saveDrill(drill: Drill) {
        console.log('Save drill:', drill);
        return this.http.put(`${environment.apiUrl}/drills/${drill.id}`, drill);
    }
    
    deleteDrill(id: number) {
        console.log('deleteDrill:', id);
        return this.http.delete(`${environment.apiUrl}/drills/${id}`);
    }

    createDrill(drill: Drill) {
        console.log('Create drill:', drill);
        return this.http.post(`${environment.apiUrl}/drill/`, drill);
    }

    getDrillById(id: number):Drill {

        console.log('Filter 1',id)
        console.log('Filter 2',this.drills)
        //console.log(this.drillService.drills.filter( drill => drill.id == id).pop())
        let d = <Drill[]>this.drills.filter( drill => drill.id == id);
        console.log('Drill:',d)

        return d.pop();
    }    

    setCachedDrill(drill: Drill) {
        console.log('setCachedDrill:', drill);
        console.log('drills 1:',this.drills);
        let idx = this.drills.findIndex(d => d.id == drill.id);
        this.drills[idx] = drill;
        this.setDrills(this.drills);
        console.log('drills 2:',this.drills);
    }

    
}