﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AccountService, AlertService } from '@app/_services';

@Component({ templateUrl: 'login.component.html' })
export class LoginComponent implements OnInit {
    form: FormGroup;
    loading = false;
    submitted = false;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private accountService: AccountService,
        private alertService: AlertService
    ) { }

    ngOnInit() {

        if (this.route.snapshot.queryParams['token']) {
            this.loading = true;

            console.log('loginByToken');
            this.accountService.loginByToken(this.route.snapshot.queryParams['token'])
            .pipe(first())
            .subscribe({
                next: (user) => {

                    if (user.id) {
                        // get return url from query parameters or default to home page
                        const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/users/edit/'+user.id;
    
                        console.log('returnUrl:',returnUrl)
                        this.router.navigateByUrl(returnUrl);
                    } else {
                        console.log("Next..", user);
                        this.loading = false;
                        localStorage.removeItem('user');
                        this.router.navigate(['../login'], { relativeTo: this.route });
                        this.alertService.error('Ongeldige login')
                    }
                    return;

                },
                error: error => {
                    console.log("error:",'error');
                    this.alertService.error('error');
                    this.loading = false;
                }
            });            
        }
        console.log('login:',this.route.snapshot.queryParams)
        this.form = this.formBuilder.group({
            email: ['', Validators.required],
            password: ['', Validators.required]
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.form.controls; }

    onSubmit() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.form.invalid) {
            return;
        }

        this.loading = true;
        this.accountService.login(this.f.email.value, this.f.password.value)
            .pipe(first())
            .subscribe({
                next: (user) => {

                    if (user.id) {
                        // get return url from query parameters or default to home page
                        const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    
                        console.log('returnUrl:',returnUrl)
                        this.router.navigateByUrl(returnUrl);
                    } else {
                        console.log("Next..", user);
                        this.loading = false;
                        localStorage.removeItem('user');
                        this.router.navigate(['../login'], { relativeTo: this.route });
                        this.alertService.error('Ongeldige login')
                    }
                    return;

                },
                error: error => {
                    console.log("error:",'error');
                    this.alertService.error('error');
                    this.loading = false;
                }
            });
    }
}